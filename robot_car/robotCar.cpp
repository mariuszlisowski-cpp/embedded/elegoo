#include "robotCar.hpp"
#include <ostream>

using namespace boost;

const char* BT_DEVICE = "/dev/tty.HC-05-SPPDev";

RobotCar::RobotCar() {
    sAppName = "ELEGOO Smart Robot Car (Kit V3.0 Plus) control";
}

bool RobotCar::OnUserCreate() {
    port = std::make_unique<asio::serial_port>(context);
    system::error_code errCode;
    port->open(BT_DEVICE, errCode);
    if (errCode) {
        std::cerr << errCode.message() << std::endl;
        return false;
    }
    if (port->is_open()) {
        port->set_option(asio::serial_port::baud_rate(9600));
        port->set_option(asio::serial_port::character_size());
        port->set_option(asio::serial_port::stop_bits());
        port->set_option(asio::serial_port::parity());
        port->set_option(asio::serial_port::flow_control());
        AsyncReadFromPort();
        thrContext = std::thread([&] { context.run(); });
    }

    btnForward.text = "Forward";
    btnForward.verticalPos = { 78, 10 };
    btnForward.verticalSize = { 100, 30 };
    btnBackward.text = "Backward";
    btnBackward.verticalPos = { 78, 110 };
    btnBackward.verticalSize = { 100, 30 };
    btnLeft.text = "Turn Left";
    btnLeft.verticalPos = { 10, 60 };
    btnLeft.verticalSize = { 100, 30 };
    btnRight.text = "Turn Right";
    btnRight.verticalPos = { 146, 60 };
    btnRight.verticalSize = { 100, 30 };

    btnForward.draw(this);
    btnBackward.draw(this);
    btnLeft.draw(this);
    btnRight.draw(this);

    return true;
}

bool RobotCar::OnUserDestroy() {
    if (port) {
        port->cancel();
        port->close();
    }
    if (thrContext.joinable()) {
        thrContext.join();
    }
    port.reset();

    return true;
}
 
void RobotCar::AsyncReadFromPort() {
    port->async_read_some(asio::buffer(&incomingByte, 1),
                          [this](system::error_code error, size_t length) {
                              if (!error) {
                                  incomingData.append(1, incomingByte);
                                  std::cout << "byte: " << incomingByte << std::endl;
                                  AsyncReadFromPort();
                              }
                          });
}

bool RobotCar::OnUserUpdate(float elapsedTime) {
    system::error_code errorCode;
    if (GetMouse(0).bPressed ||
        GetKey(olc::UP).bPressed ||
        GetKey(olc::DOWN).bPressed ||
        GetKey(olc::LEFT).bPressed ||
        GetKey(olc::RIGHT).bPressed)
    {
        port->write_some(asio::buffer("s", 1), errorCode);
        std::this_thread::sleep_for(std::chrono::milliseconds(250));
        if (btnForward.clicked(GetMousePos()) ||
            GetKey(olc::UP).bPressed)
        {
            btnForward.showClicked(this);
            port->write_some(asio::buffer("f", 1), errorCode);
            std::cout << "> forward " << std::flush;
            if (errorCode) {
                std::cerr << "Error movig forward" << std::endl;
            }
        }
        if (btnBackward.clicked(GetMousePos()) ||
            GetKey(olc::DOWN).bPressed)
        {
            btnBackward.showClicked(this);
            port->write_some(asio::buffer("b", 1), errorCode);
            std::cout << "> backward " << std::flush;
            if (errorCode) {
                std::cerr << "Error movig backward" << std::endl;
            }
        }
        if (btnLeft.clicked(GetMousePos()) ||
            GetKey(olc::LEFT).bPressed)
        {
            btnLeft.showClicked(this);
            std::cout << "> left " << std::flush;
            port->write_some(asio::buffer("l", 1), errorCode);
            if (errorCode) {
                std::cerr << "Error movig left" << std::endl;
            }
        }
        if (btnRight.clicked(GetMousePos()) ||
            GetKey(olc::RIGHT).bPressed)
        {
            btnRight.showClicked(this);
            std::cout << "> right " << std::flush;
            port->write_some(asio::buffer("r", 1), errorCode);
            if (errorCode) {
                std::cerr << "Error movig right" << std::endl;
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(250));
    }
    if (GetMouse(0).bReleased ||
        GetKey(olc::UP).bReleased ||
        GetKey(olc::DOWN).bReleased ||
        GetKey(olc::LEFT).bReleased ||
        GetKey(olc::RIGHT).bReleased)
    {
        port->write_some(asio::buffer("s", 1), errorCode);
        std::this_thread::sleep_for(std::chrono::milliseconds(250));
        if (errorCode) {
            std::cerr << "Error stopping" << std::endl;
        }
        std::cout << "- released" << std::endl;
        Clear(olc::BLACK);
        btnForward.draw(this);
        btnBackward.draw(this);
        btnLeft.draw(this);
        btnRight.draw(this);
    }
    
    return true;
}

bool RobotCar::Button::clicked(const olc::vi2d& mousePos) {
    return mousePos.x >= verticalPos.x &&
           mousePos.x < (verticalPos.x + verticalSize.x) &&
           mousePos.y >= verticalPos.y &&
           mousePos.y < (verticalPos.y + verticalSize.y);
}

void RobotCar::Button::draw(olc::PixelGameEngine* pge) {
    pge->FillRect(verticalPos, verticalSize, olc::BLUE);
    pge->DrawRect(verticalPos, verticalSize, olc::WHITE);
    olc::vi2d textSize = pge->GetTextSizeProp(text);
    pge->DrawStringProp(verticalPos + (verticalSize - textSize) / 2, text, olc::WHITE);
}

void RobotCar::Button::showClicked(olc::PixelGameEngine* pge) {
    pge->DrawRect(verticalPos, verticalSize, olc::RED);
}

int main() {
    RobotCar car;
    if (car.Construct(256, 156, 4, 4)) {
        car.Start();
    }

    return 0;   
}
