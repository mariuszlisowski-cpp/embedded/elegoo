#pragma once

#define OLC_PGE_APPLICATION
#include "olcPixelGameEngine.h"

#include "boost/asio/io_context.hpp"
#include "boost/asio/serial_port.hpp"
#include "boost/asio/buffer.hpp"
#include "boost/asio/serial_port.hpp"
#include "boost/system/error_code.hpp"

#include <memory>
#include <string>
#include <thread>

using namespace boost;

class RobotCar : public olc::PixelGameEngine {
public:
    RobotCar();

    bool OnUserCreate() override;
    bool OnUserDestroy() override;
    void AsyncReadFromPort();
    bool OnUserUpdate(float fElapsedTime) override;

    struct Button {
        bool clicked(const olc::vi2d& mousePos);
        void draw(olc::PixelGameEngine* pge);
        void showClicked(olc::PixelGameEngine* pge);
        
        olc::vi2d verticalPos;
        olc::vi2d verticalSize;
        std::string text;
    };

private:
    asio::io_context context;
    std::unique_ptr<asio::serial_port> port;
    char incomingByte;
    std::string incomingData;
    std::thread thrContext;
    Button btnForward;
    Button btnBackward;
    Button btnLeft;
    Button btnRight;
};
